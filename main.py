import time
import pygame
import numpy as np

COLOR_GRID = (30, 30, 30)
COLOR_BG = (10, 10, 100)
COLOR_DIE_NEXT = (170, 170, 170)
COLOR_ALIVE_NEXT = (255, 255, 25)

WIDTH = 600
HEIGHT = 600

CELL_SIZE = 10

NB_CELL_X = int(WIDTH/CELL_SIZE)
NB_CELL_Y = int(HEIGHT/CELL_SIZE)

MARGE = 2

def update(screen, grid, with_progress=False):
    updated_grid = np.zeros_like(grid)
    rows, cols = grid.shape

    for row in range(1, rows - 1):
        for col in range(1, cols - 1):
            alive = np.sum(grid[row-1:row+2, col-1:col+2]) - grid[row, col]
            color = COLOR_ALIVE_NEXT if grid[row, col] else COLOR_BG
            
            if grid[row, col]:
                if alive == 2 or alive == 3:
                    updated_grid[row, col] = 1
                    if with_progress:
                        color = COLOR_ALIVE_NEXT
                elif with_progress:
                    color = COLOR_DIE_NEXT
            elif alive == 3:
                updated_grid[row, col] = 1
                if with_progress:
                    color = COLOR_ALIVE_NEXT
            
            pygame.draw.rect(screen, color, (col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE - MARGE, CELL_SIZE - MARGE))
    
    return updated_grid

def main():
    # Initialization
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    grid = np.zeros((NB_CELL_Y, NB_CELL_X))
    screen.fill(COLOR_GRID)
    update(screen, grid)
    pygame.display.flip() 
    pygame.display.update()
    running = False
    step = False

    # Infinite loop
    while True:
        for event in pygame.event.get():
            # To exit
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            # To launch / To stop the time
            elif event.type == pygame.KEYDOWN:
                # To launch / To stop the time
                if event.key == pygame.K_SPACE:
                    running = not running
                    update(screen, grid)
                # To launch the time step-by-step
                elif event.key == pygame.K_s:
                    update(screen, grid)
                    step = True
                # To clear the grid
                elif event.key == pygame.K_c:
                    grid = np.zeros((NB_CELL_Y, NB_CELL_X))
                    screen.fill(COLOR_GRID)
                    update(screen, grid, False)
                    running = False
            # To create life
            elif pygame.mouse.get_pressed()[0]:   # Left click
                pos = pygame.mouse.get_pos()
                col = pos[0] // CELL_SIZE
                row = pos[1] // CELL_SIZE
                
                grid[row, col] = 1
                update(screen, grid)
            # To kill cell
            elif pygame.mouse.get_pressed()[2]:   # Right click
                pos = pygame.mouse.get_pos()
                col = pos[0] // CELL_SIZE
                row = pos[1] // CELL_SIZE
                grid[row, col] = 0
                update(screen, grid)
            
            pygame.display.update()

        if running:
            grid = update (screen, grid, with_progress=True)
            pygame.display.update()
            time.sleep(0.01)
        elif step:
            grid = update (screen, grid, with_progress=True)
            pygame.display.update()
            step = False
            time.sleep(0.01)
        else:
            time.sleep(0.001)

if __name__ == '__main__':
    main()