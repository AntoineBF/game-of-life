# GAME OF LIFE

*Notice*: The code is inspired by [NeuralNine's code](https://www.youtube.com/watch?v=cRWg2SWuXtM).

## Description

The **Game of Life**, imagined by mathematician John Conway in 1970, is a **cellular automaton** that operates on a grid of cells. Each cell can be in one of two states, **alive** or **dead**, and its state is determined by two simple rules:

**Survival rule**: A living cell with **2 or 3** neighboring living cells **survives** to the next generation. If a living cell has **fewer** than 2 or **more** than 3 living neighbors, it **dies** in the next generation due to **underpopulation** or **overpopulation**, respectively.

**Reproduction rule**: An empty cell (dead or not yet used) with **exactly 3** neighboring living cells becomes **alive** in the next generation. If a dead / unused cell has any other number of living neighbors, it **remains** dead / unused.

## Various interesting structures and patterns

### Still lifes

| Names | Shapes |
|:---|:---|
| Block | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Game_of_life_block_with_border.svg/66px-Game_of_life_block_with_border.svg.png" alt="drawing" height="200"/> |
| Bee-hive | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Game_of_life_beehive.svg/98px-Game_of_life_beehive.svg.png" alt="drawing" height="200"/> |
| Loaf | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Game_of_life_loaf.svg/98px-Game_of_life_loaf.svg.png" alt="drawing" height="200"/> |
| Boat | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Game_of_life_boat.svg/1024px-Game_of_life_boat.svg.png" alt="drawing" height="200"/> |
| Tub | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Game_of_life_flower.svg/82px-Game_of_life_flower.svg.png" alt="drawing" height="200"/> |
| Frame | ![Frame](./img/frame.png) |

### Oscillators

| Names | Shapes |
|:---|:---|
| Blinker (period 2) | <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/Game_of_life_blinker.gif" alt="drawing" height="200"/> |
| Toad (period 2) | <img src="https://upload.wikimedia.org/wikipedia/commons/1/12/Game_of_life_toad.gif" alt="drawing" height="200"/> |
| Beacon (period 2) | <img src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Game_of_life_beacon.gif" alt="drawing" height="200"/> |
| Pulsar (period 3) | <img src="https://upload.wikimedia.org/wikipedia/commons/0/07/Game_of_life_pulsar.gif" alt="drawing" height="200"/> |
| Penta-decathlon (period 15) | <img src="https://upload.wikimedia.org/wikipedia/commons/f/fb/I-Column.gif" alt="drawing" height="200"/> |
| Hash (period 5) | ![Hash p5](./img/hash5.gif) |

### Spaceships

| Names | Shapes |
|:---|:---|
| Glider | <img src="https://upload.wikimedia.org/wikipedia/commons/f/f2/Game_of_life_animated_glider.gif" alt="drawing" height="200"/> |
| Light-weight spaceship (LWSS) | <img src="https://upload.wikimedia.org/wikipedia/commons/3/37/Game_of_life_animated_LWSS.gif" alt="drawing" height="200"/> |
| Middle-weight spaceship (MWSS) | <img src="https://upload.wikimedia.org/wikipedia/commons/4/4e/Animated_Mwss.gif" alt="drawing" height="200"/> |
| Heavy-weight spaceship (HWSS) | <img src="https://upload.wikimedia.org/wikipedia/commons/4/4f/Animated_Hwss.gif" alt="drawing" height="200"/> |

*Notice*: For those who have illustrations in blue and yellow, I haven't found their names on the internet, so I don't know if they've been declared. 
